package com.example.floating_button

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import com.yhao.floatwindow.FloatWindow
import com.yhao.floatwindow.Screen

import io.flutter.app.FlutterActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant

class MainActivity: FlutterActivity() {

  companion object {
    private @JvmStatic val CHANNEL: String = "floating_button"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    GeneratedPluginRegistrant.registerWith(this)

    val channel: MethodChannel = MethodChannel(flutterView, CHANNEL)

    channel.setMethodCallHandler{
      call, result ->
        when (call.method) {
          "create" -> {
            val imageView = ImageView(applicationContext)
            imageView.setImageResource(R.drawable.plus)

            FloatWindow.with(applicationContext).setView(imageView)
                    .setWidth(Screen.width, 0.15f)
                    .setHeight(Screen.width, 0.15f)
                    .setX(Screen.width, 0.15f)
                    .setY(Screen.height, 0.15f)
                    .setDesktopShow(true)
                    .build()

            imageView.setOnClickListener { channel.invokeMethod("touch", null) }
          }
          "show" -> {
            FloatWindow.get().show()
          }
          "hide" -> {
            FloatWindow.get().hide()
          }
          "isShowing" -> {
            result.success(FloatWindow.get().isShowing)
          }
          else -> result.notImplemented()
        }

    }
  }

  override fun onDestroy() {
    FloatWindow.destroy()
    super.onDestroy()
  }
}
